# Run with 'awk -f correct_msf_question.awk quiz_in.csv quiz_out.csv'
# Ensure quiz is exported with & delimiter.
# $6 is total and $17 is question.
# Sort out 1st incorrect question
BEGIN {FS="&"; OFS="&"} 
{
  if ($17 == 7)
    $6 = $6 + 1;
  else if ($17 == 2)
    $6 = $6 - 1;
}


