# Update Git Folders
# by Paul Livesey

update_folder() {
  cd "$1"
  git pull
  git add .
  echo "What is the message for folder $1?"
  read message
  git commit -m "$message"
  git push -u $2 master
}

# Update org files
update_folder "~/org" "git@gitlab.com:Liveo13/org-mode-files.git"

# Update share files
update_folder "~/share" "git@gitlab.com:Liveo13/share.git"

# Update .doom.d Emacs folder
update_folder "~/.doom.d" "git@gitlab.com:Liveo13/doomd.git"

# Update roam folder
update_folder "~/roam" "git@gitlab.com:Liveo13/roam.git"

# Update scripts folder
update_folder "~/script" "git@gitlab.com:Liveo13/scripts.git"

