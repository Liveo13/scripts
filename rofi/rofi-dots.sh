#!/bin/bash

# from https://gitlab.com/OldTechBloke/rofi-edit/-/raw/main/rofi-edit 

myTerm=urxvt
myEdit=nano

menu(){
	printf "1. alacritty\n"
	printf "2. awesome\n"
	printf "3. sxhkd\n"
	printf "4. tmux\n"
	printf "5. espanso\n"
	printf "6. rofi\n"
	printf "7. vifm\n"
	printf "8. bashrc\n"
	printf "9. profile\n"
	printf "10. zshrc\n"
}

main(){
    choice=$(menu | rofi -dmenu | cut -d. -f1)

	case $choice in
		1)
			$myTerm -e sh -c "sleep 0.2 ; vim $HOME/.config/alacritty/alacritty.yml"
            ;;
		2)
			$myTerm -e sh -c "sleep 0.2 ; vim $HOME/.config/awesome/rc.lua"
            ;;
		3)
			$myTerm -e sh -c "sleep 0.2 ; vim $HOME/.config/sxhkd/sxhkdrc"
            ;;
		4)
           	$myTerm -e sh -c "sleep 0.2 ; vim $HOME/.tmux.conf"
            ;;
		5)
            $myTerm -e sh -c "sleep 0.2 ; vim $HOME/.config/espanso/default.yml"
            ;;
		6)
            $myTerm -e sh -c "sleep 0.2 ; vim $HOME/.config/rofi/config.rasi"
            ;;
		7)
           	$myTerm -e sh -c "sleep 0.2 ; vim $HOME/.config/vifm/vifmrc"
            ;;
		8)
            $myTerm -e sh -c "sleep 0.2 ; vim $HOME/.bashrc"
            ;;
		9)
            $myTerm -e sh -c "sleep 0.2 ; vim $HOME/.profile"
            ;;
		10)
            $myTerm -e sh -c "sleep 0.2 ; vim $HOME/.zshrc"
            ;;

        esac
}

main

