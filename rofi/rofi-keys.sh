#!/bin/bash

# from https://gitlab.com/OldTechBloke/rofi-edit/-/raw/main/rofi-edit 

myTerm=alacritty
myEdit=vim

menu(){
	printf "1. bash/zsh\n"
	printf "2. emacs\n"
	printf "3. vim\n"
	printf "4. tmux\n"
	printf "5. alacritty\n"
	printf "6. qutebrowser\n"
	printf "7. vifm\n"
	printf "8. terminator\n"
}

main(){
    choice=$(menu | rofi -dmenu | cut -d. -f1)

	case $choice in
		1)
            $myTerm -e sh -c "sleep 0.2 ; vim $HOME/.bash_keys.txt"
            ;;
		2)
			$myTerm -e sh -c "sleep 0.2 ; vim $HOME/.config/awesome/rc.lua"
            ;;
		3)
			$myTerm -e sh -c "sleep 0.2 ; vim $HOME/.config/sxhkd/sxhkdrc"
            ;;
		4)
           	$myTerm -e sh -c "sleep 0.2 ; vim $HOME/.tmux.conf"
            ;;
		5)
			      $myTerm -e sh -c "sleep 0.2 ; less $HOME/.config/alacritty/.alacritty_keys.txt"
            ;;
		6)
            $myTerm -e sh -c "sleep 0.2 ; vim $HOME/.config/rofi/config.rasi"
            ;;
		7)
           	$myTerm -e sh -c "sleep 0.2 ; vim $HOME/.config/vifm/vifmrc"
            ;;
		8)
            $myTerm -e sh -c "sleep 0.2 ; vim $HOME/.bashrc"
            ;;
		9)
            $myTerm -e sh -c "sleep 0.2 ; vim $HOME/.profile"
            ;;
		10)
            $myTerm -e sh -c "sleep 0.2 ; vim $HOME/.zshrc"
            ;;

        esac
}

main

